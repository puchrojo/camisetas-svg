
## camisetas-svg

Colección de mis camisetas en vector format svg, directas para imprimir.


## Software
Los vectores han sido creados con inkscape, ya que con gimp los vectores no se trabajan bien.
Se abre la foto (jpg/png) de la camiseta con inkscape y se van creando capas con los vectores por encima de la foto.
Aunque la malloría de las camisetas son con tinta blanca sobre fondo negro, en la tienda prefiren la tinta en negro sin fondo.
Ellos cambian el color.

## Tiendas

### Magic Print (Bremen)

T-Shirt Drucken  <info@magic-print.de>
+49-421-3365100
Kahlenstraße 7
28195 Bremen
www.magic-print.de
